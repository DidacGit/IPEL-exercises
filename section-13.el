;;; section-13.el --- Exercises for the section 13

;;; Commentary:
;; Exercises with counting

;;; Code:

;; 1) Using a ‘while’ loop, write a function to count the number of
;; punctuation marks in a region—period, comma, semicolon, colon,
;; exclamation mark, and question mark.  Do the same using recursion.

(defun my-count-punctuation-maks (beginning end)
  "Count the number of punctuation marks in the region."
  (interactive "r")
  (save-excursion
    (goto-char beginning)
    (let ((count 0))
      (while (re-search-forward "[[:punct:]]" end t)
	(incf count))
      (cond ((zerop count)
	     (message "The region does NOT have any punctuation marks."))
	    ((= 1 count)
	     (message "The region has 1 punctuation mark."))
	    (t
	     (message "The region has %d punctuation marks." count))))))

(defun my-count-punctuation-marks-recursively (beginning end)
  "Count the number of punctuation marks in the region."
  (interactive "r")
  (defun iter (count)
    (if (re-search-forward "[[:punct:]]" end t)
	(iter (1+ count))
      count))
  (save-excursion
    (goto-char beginning)
    (let ((count (iter 0)))
      (cond ((zerop count)
	     (message "The region does NOT have any punctuation marks."))
	    ((= 1 count)
	     (message "The region has 1 punctuation mark."))
	    (t
	     (message "The region has %d punctuation marks." count))))))

(provide 'section-13)
;;; section-13.el ends here
