;;; section-4.el --- Exercises for the section 4

;;; Commentary:
;; Exercises about buffer editing functions

;;; Code:

;; 1) Write your own ‘simplified-end-of-buffer’ function definition;
;; then test it to see whether it works.

(defun simplified-end-of-buffer ()
  "Move point to the end of the buffer.
Also leave mark at the previous position."
  (interactive)
  (push-mark)
  (goto-char (point-max)))

;; 2) Use ‘if’ and ‘get-buffer’ to write a function that prints a
;; message telling you whether a buffer exists.

(defun buffer-exists? (buffer)
  "Tell whether the BUFFER exists."
  (interactive "BEnter the buffer name: ")
  (if (get-buffer buffer)
      (message "The buffer %s exists" buffer)
    (message "The buffer %s doesn't exist" buffer)))

;; 3) Using ‘xref-find-definitions’, find the source for the
;; ‘copy-to-buffer’ function.

;; Done by pressing "M-." when point is on the function name.

(provide 'section-4)
;;; section-4.el ends here
