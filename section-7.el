;;; section-7.el --- Exercises for the section 7

;;; Commentary:
;; Exercises about car, cdr and cons

;;; Code:

;; Construct a list of four birds by evaluating several expressions
;; with ‘cons’.  Find out what happens when you ‘cons’ a list onto
;; itself.  Replace the first element of the list of four birds with a
;; fish.  Replace the rest of that list with a list of other fish.

(setq birds (cons 'parrot (cons 'eagle (cons 'crow '(pigeon))))) ;; '(parrot eagle crow pigeon)
(cons birds birds) ;; '((parrot eagle crow pigeon) parrot eagle crow pigeon)

(setcar birds 'sardine) ;; birds: '(sardine eagle crow pigeon)
(setcdr birds '(carp goldfish trout));; birds '(sardine carp goldfish trout)

(provide 'section-7)
;;; section-7.el ends here
