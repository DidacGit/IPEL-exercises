;;; section-1.el --- Exercises for the section 1

;;; Commentary:
;; Exercises about basic Lisp features.

;;; Code:

;; 1) Generate an error message by evaluating an appropriate symbol that
;; is not within parentheses.

;; "forward-char" Symbol’s value as variable is void: forward-char

;; 2) Generate an error message by evaluating an appropriate symbol that
;; is between parentheses.

;; "(fill-column)" Symbol’s function definition is void: fill-column

;; 3) Create a counter that increments by two rather than one.
(setq counter 0)
(setq counter (+ counter 2))
counter

;; 4) Write an expression that prints a message in the echo area when
;; evaluated

(message "The current point is: %d" (point))

(provide 'section-1)
;;; section-1.el ends here
