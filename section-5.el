;;; section-5.el --- Exercises for the section 5

;;; Commentary:
;; Exercises about more complex buffer editing functions

;;; Code:

;; Write an interactive function with an optional argument that tests
;; whether its argument, a number, is greater than or equal to, or
;; else, less than the value of ‘fill-column’, and tells you which, in
;; a message.  However, if you do not pass an argument to the
;; function, use 56 as a default value.

(defun compare-arg-to-fill-column (&optional arg)
  "Test ARG against the value of 'fill-column'.
If ARG is nil, use 56 instead."
  (interactive "P")
  (setq arg (or arg 56))
  (cond ((= arg fill-column)
	 (message "The argument (%s) is equal to 'fill-column' (%s)." arg fill-column))
	((> arg fill-column)
	 (message "The argument (%s) is greater than 'fill-column' (%s)." arg fill-column))
	(t
	 (message "The argument (%s) is lower than 'fill-column' (%s)." arg fill-column))))

(provide 'section-5)
;;; section-5.el ends here
