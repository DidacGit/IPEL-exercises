;;; section-14.el --- Exercises for the section 14

;;; Commentary:
;; Exercise of a graph showing the most common lengths of the
;; functions of a directory.

;;; Code:

(defun count-words-in-defun ()
  "Return the number of words and symbols in a defun."
  (cl-loop initially (beginning-of-defun)
     with end = (save-excursion (end-of-defun) (point))
     while (< (point) end)
     count (re-search-forward
	    "\\(\\w\\|\\s_\\)+[^ \t\n]*[ \t\n]*"
	    end t)))

(defun lengths-list-file (filename)
  "Return list of definitions' lengths within FILENAME.

The returned list is a list of numbers.  Each number is the
number of words or symbols in one function definition."
  (message "Working on `%s' ... " filename)
  (save-excursion
    (cl-loop with buffer = (find-file-noselect filename)
       initially
	 (set-buffer buffer)
	 (setf buffer-read-only t)
	 (widen)
       while (re-search-forward "^(defun" nil t)
       collect (count-words-in-defun)
       finally (kill-buffer buffer))))

(defun lengths-list-many-files (list-of-files)
  "Return list of lengths of defuns in LIST-OF-FILES."
  (let (lengths)
    (dolist (file list-of-files lengths)
      (setq lengths (nconc lengths (lengths-list-file (expand-file-name file)))))))

(defun files-in-below-directory (directory)
  "List the .el files in DIRECTORY and in its sub-directories."
  (let (el-files)
    (dolist (file (directory-files-and-attributes directory t) el-files)
      (let ((name (first file))
	    (type (second file)))
	(if (equal ".el" (substring name -3))
	    (setq el-files (cons name el-files))
	  ;; if it's a directory, and it's not ".", ".." or ".*"
	  (when (and (eq t type)
		     (not (equal "." (substring name -1))))
	    (setq el-files (append el-files (files-in-below-directory name)))))))
    el-files))

(defvar top-of-ranges
  '(10  20  30  40  50
    60  70  80  90 100
    110 120 130 140 150
    160 170 180 190 200
    210 220 230 240 250
    260 270 280 290 300)
  "List specifying ranges for `defuns-per-range'.")

(defun defuns-per-range (sorted-lengths top-of-ranges)
  "Lengths of SORTED-LENGTHS in each TOP-OF-RANGES range."
  (let ((defuns-per-range-list)
	(lengths-in-range 0))
    ;; Iterate each range, adding the number of defuns in that range to the list
    (dolist (top-of-range top-of-ranges)
      (while (and sorted-lengths
		  (< (first sorted-lengths) top-of-range))
	(incf lengths-in-range)
	(setf sorted-lengths (cdr sorted-lengths)))
      ;; Push the value and reset it
      (push lengths-in-range defuns-per-range-list)
      (setf lengths-in-range 0))
    ;; Add any extra lengths outside the last range (if there are any)
    (push (length sorted-lengths) defuns-per-range-list)
    (nreverse defuns-per-range-list)))

;; (defuns-per-range-old (sort (lengths-list-many-files (files-in-below-directory "test")) '<) '(20 30 33 35)) ;; (2 9 1 1 4)
;; (defuns-per-range (sort (lengths-list-many-files (files-in-below-directory "test")) '<) '(20 30 33 35)) ;; (2 9 1 1)

;; Disable indenting modes such as `aggressive-indent-mode' to test it
(cl-defun print-graph (numbers-list &key (graph-symbol "*")
				      (graph-blank " ")
				      (y-axis-spacing 5)
				      (y-axis-tic " - ")
				      (x-axis-tic "|"))
  "Print a bar graph of the NUMBERS-LIST.

The numbers-list consists of the Y-axis values.  "
  (save-excursion
    (let ((height (apply 'max numbers-list))
	  (symbol-width (length graph-blank)))
      ;; 1) Generate the y-axis
      (insert-rectangle
       (cl-labels ((is-label? (y-value)
		     ;; E.g. for 5: 5, 10, 15, etc.
		     (zerop (% y-value y-axis-spacing)))
		   (create-label (number)
		     ;; E.g. for 54: "54 - "
		     (concat (number-to-string number) y-axis-tic))
		   (length-of-label (number)
		     ;; E.g. for 54: "54 - ", length = 5
		     (length (create-label number))))
	 (let* ((height-top-of-line
		 ;; E.g. if the highest number is 12, and y-axis-spacing is 5, the height is 15
		 (if (is-label? height)
		     height
		   (* (1+ (/ height y-axis-spacing))
		      y-axis-spacing)))
		;; to be aligned, all labels must have the same width (shorter ones will have leading spaces)
		(label-width (length-of-label height-top-of-line)))
	   (cl-loop for current-y from height-top-of-line downto 1
	      if (or (= current-y 1)
		     ;; Add the necessary spaces
		     (is-label? current-y))
	      ;; Add spaces in front if necessary
	      collect (concat (make-string (- (length-of-label height-top-of-line)
					      (length-of-label current-y))
					   ? )
			      (create-label current-y))
	      else collect " "))))
      
      ;; 2) Generate the x-axis
      (let* ((x-axis-label-spacing (* 5 (length graph-blank)))
	     (full-y-label-width 5) ;; TODO
	     (x-axis-tic-element (concat (make-string
					  ;; Make a string of blanks.
					  (-  (* symbol-width x-axis-label-spacing)
					      (length x-axis-tic))
					  ? )
					 x-axis-tic))
	     (x-axis-leading-spaces (make-string full-y-label-width ? ))
	     (x-length (length numbers-list))
	     (tic-width (* symbol-width x-axis-label-spacing))
	     (number-of-x-tics (if (zerop (% x-length tic-width))
				   (/ (X-length tic-width))
				 (1+ (/ (X-length tic-width))))))
	;; 2.1) Print the x-axis tic line
	(insert (make-string full-y-label-width ? )
		x-axis-tic
		;; Insert the white space up to the second tic
		(concat (make-string
			 (-  (* symbol-width x-axis-label-spacing)
			     (* 2 (length x-axis-tic)))
			 ? )
			x-axis-tic))
	;; Insert the remaining tics
	(cl-loop for i from 1 to number-of-x-tics
	   do (insert x-axis-tic-element))
	;; 2.2) Print the x-axis number line
	(insert x-axis-leading-spaces
		"1"
		(concat (make-string
			 ;; Insert white space up to next number
			 (-  (* symbol-width x-axis-label-spacing) 2)
			 ? )
			(number-to-string number)))
	;; 2) Generate the x-axis
	(let ((x-length (length numbers-list))
	      (x-axis-label-spacing (* 5 (length graph-blank)))
	      (tic-width (* symbol-width x-axis-label-spacing)))
	  (cl-loop
	     for i from 1 to (if (zerop (% x-length tic-width))
				 (/ (x-length tic-width))
			       (1+ (/ (x-length tic-width))))
	     ;; The first segment is one space smaller
	     if (= i 1) collect (concat)
	     else collect (concat ))))
      ;; 3) Generate the graph
      ;; Iterate the list, inserting each column one at a time
      (dolist (y numbers-list)
	(save-excursion
	  (insert-rectangle
	   ;; Return a list representing the current column (x)
	   (cl-loop for current-y from height downto 1
	      if (> current-y y) collect graph-blank
	      else collect graph-symbol)))
	;; Position the point so it's ready to insert the next rectangle
	(forward-char symbol-width)))))

(provide 'section-14)
;;; section-14.el ends here
