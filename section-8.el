;;; section-8.el --- Exercises for the section 8

;;; Commentary:
;; Exercises about searching.

;;; Code:

;; 1) Write an interactive function that searches for a string.  If the
;; search finds the string, leave point after it and display a message
;; that says “Found!”.  (Do not use ‘search-forward’ for the name of
;; this function; if you do, you will overwrite the existing version
;; of ‘search-forward’ that comes with Emacs.  Use a name such as
;; ‘test-search’ instead.)

(defun test-search (strng)
  "Search for STRNG in the text."
  (interactive "sSearch: ")
  (when (search-forward strng)
    (message "Found!")))

;; 2) Write a function that prints the third element of the kill ring in
;; the echo area, if any; if the kill ring does not contain a third
;; element, print an appropriate message.

(defun print-3-kill-ring ()
  "Print the 3rd element of the `kill-ring'."
  (interactive)
  (let ((3-element (nth 2 kill-ring)))
    (if 3-element
	(message "%s" 3-element)
      (message "The kill-ring doesn't have a third element"))))

(provide 'section-8)
;;; section-8.el ends here
