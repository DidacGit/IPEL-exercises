;;; section-10.el --- Exercises for the section 10

;;; Commentary:
;; Exercises about yanking.

;;; Code:

;; 1) Using ‘C-h v’ (‘describe-variable’), look at the value of your
;; kill ring.  Add several items to your kill ring; look at its value
;; again.  Using ‘M-y’ (‘yank-pop)’, move all the way around the kill
;; ring.  How many items were in your kill ring?  Find the value of
;; ‘kill-ring-max’.  Was your kill ring full, or could you have kept
;; more blocks of text within it?

kill-ring-max ;; "60" it was not yet full

;; 2) Using ‘nthcdr’ and ‘car’, construct a series of expressions to
;; return the first, second, third, and fourth elements of a list.

(setq my-list '(hello there my friend))
(car my-list) ;; hello
(car (nthcdr 1 my-list)) ;; there
(car (nthcdr 2 my-list)) ;; my
(car (nthcdr 3 my-list)) ;; friend

(provide 'section-10)
;; section-10.el ends here
