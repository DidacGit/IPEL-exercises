;;; section-6.el --- Exercises for the section 6

;;; Commentary:
;; Exercises about narrowing and widening

;;; Code:

;; Write a function that will display the first 60 characters of the
;; current buffer, even if you have narrowed the buffer to its latter
;; half so that the first line is inaccessible.  Restore point, mark,
;; and narrowing.  For this exercise, you need to use a whole
;; potpourri of functions, including ‘save-restriction’, ‘widen’,
;; ‘goto-char’, ‘point-min’, ‘message’, and ‘buffer-substring’.

;; (‘buffer-substring’ is a previously unmentioned function you will
;; have to investigate yourself; or perhaps you will have to use
;; ‘buffer-substring-no-properties’ or ‘filter-buffer-substring’ ...,
;; yet other functions.  Text properties are a feature otherwise not
;; discussed here.  *Note Text Properties: (elisp)Text Properties.)

;; Additionally, do you really need ‘goto-char’ or ‘point-min’?  Or
;; can you write the function without them?

(defun display-first-60-char ()
  "Display the first 60 characters of the current buffer."
  (interactive)
  (save-restriction
    (widen)
    (message (buffer-substring-no-properties 1 60))))

(provide 'section-6)
;;; section-6.el ends here
