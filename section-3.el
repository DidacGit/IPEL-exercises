;;; section-3.el --- Exercises for the section 3

;;; Commentary:
;; Exercises about function definitions

;;; Code:

;; 1) Write a non-interactive function that doubles the value of its
;; argument, a number.  Make that function interactive.

(defun double-value (arg)
  "Double the value of ARG."
  (interactive "p")
  (message "The double of %s is: %s" arg (* arg 2)))

(double-value 3)

;; 2) Write a function that tests whether the current value of
;; ‘fill-column’ is greater than the argument passed to the function,
;; and if so, prints an appropriate message.

(defun greater-than-fill-column? (arg)
  "Test if ARG is greater than 'fill-column'."
  (if (> fill-column arg)
      (message "The value of 'fill-column' (%s) is greater than the argument (%s)." fill-column arg)))

(greater-than-fill-column? 15)

(provide 'section-3)
;;; section-3.el ends here
