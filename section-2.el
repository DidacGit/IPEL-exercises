;;; section-2.el --- Exercises for the section 2

;;; Commentary:
;; Exercises about Lisp evaluation

;;; Code:

;; Find a file with which you are working and move towards its middle.
;; Find its buffer name, file name, length, and your position in the file.

(switch-to-buffer "*scratch*")
(goto-char (/ (+ (point-min) (point-max)) 2))
(message "Buffer: %s, File: %s, Length: %d, Position: %d"
	 (buffer-name) (buffer-file-name) (buffer-size) (point))

(provide 'section-2)
;;; section-2.el ends here
