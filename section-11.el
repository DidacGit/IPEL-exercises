;;; section-11.el --- Exercises for the section 11

;;; Commentary:
;; Exercises about looping and recursion.

;;; Code:

;; 1) Write a function similar to ‘triangle’ in which each row has a
;; value which is the square of the row number.  Use a ‘while’ loop.

(defun add-squares (num)
  "Add up the squares of each number before NUM."
  (let ((total 0))
    (while (> num 0)
      (incf total (expt num 2))
      (decf num))
    total))

(add-squares 3) ;; 14 = 3*3 + 2*2 + 1*1

;; 2) Write a function similar to ‘triangle’ that multiplies instead
;; of adds the values.

(defun mult-triangle (num)
  "Multiply each number before NUM."
  (let ((total 1))
    (while (> num 0)
      (setq total (* total num))
      (decf num))
    total))

(mult-triangle 4) ;; 1*2*3*4 = 24

;; 3) Rewrite these two functions recursively.  Rewrite these
;; functions using ‘cond’.

(defun add-squares-recur (num)
  "Add up the squares of each number up to NUM, recursively."
  (defun iter (count total)
    (if (> count num)
	total
      (iter (1+ count) (+ total (expt count 2)))))
  (iter 0 0))

(add-squares-recur 3) ;; 14 = 3*3 + 2*2 + 1*1

(defun mult-triangle-recur (num)
  "Multiply each number before NUM, recursively."
  (defun iter (count total)
    (cond ((= num 0) 0)
	  ((> count num) total)
	  (t (iter (1+ count) (* total count)))))
  (iter 1 1))

(mult-triangle-recur 4) ;; 24 = 1*2*3*4

;; 4) Write a function for Texinfo mode that creates an index entry at
;; the beginning of a paragraph for every ‘@dfn’ within the paragraph.
;; (In a Texinfo file, ‘@dfn’ marks a definition.  This book is
;; written in Texinfo.)

;; Many of the functions you will need are described in two of the
;; previous chapters, *note Cutting and Storing Text: Cutting &
;; Storing Text, and *note Yanking Text Back: Yanking.  If you use
;; ‘forward-paragraph’ to put the index entry at the beginning of the
;; paragraph, you will have to use ‘C-h f’ (‘describe-function’) to
;; find out how to make the command go backwards.

;; For more information, see *note Indicating Definitions:
;; (texinfo)Indicating.

;; More info on these definitions here: https://www.gnu.org/software/texinfo/manual/texinfo/html_node/_0040dfn.html
;; E.g. for "@dfn{deleting}" the index entry must be: "@cindex deleting".

(defun create-index-entry-for-this-paragraph ()
  "Create an index entry at the beginning of the paragraph for each '@dfn'."
  (interactive)
  (save-excursion
    (save-restriction
      (mark-paragraph)
      (narrow-to-region (point) (mark))
      (let (definitions)
	(while (search-forward "@dfn{" nil t) ; if no match, return nil
	  (push-mark)
	  (search-forward "}")
	  (backward-char)
	  (setf definitions
		(cons (buffer-substring (mark) (point)) definitions)))
	(setf definitions (nreverse definitions))
	;; (print definitions)
	(goto-char (point-min))
	(dolist (definition definitions)
	  (insert (concat "@cindex " definition "\n")))))))

;; It can be tested on a paragraph like this:

;; Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
;; eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim
;; ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
;; aliquip ex ea commodo consequat.  Duis aute irure dolor in
;; reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
;; pariatur.  Excepteur sint @dfn{occaecat} cupidatat non proident, sunt in
;; culpa qui officia deserunt mollit anim id est laborum.  Sed ut
;; perspiciatis unde omnis iste natus error sit voluptatem accusantium
;; doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
;; inventore veritatis et quasi architecto beatae vitae dicta sunt
;; explicabo.  Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut
;; odit aut fugit, sed quia consequuntur magni dolores eos qui ratione
;; voluptatem sequi nesciunt.  Neque @dfn{porro} quisquam est, qui dolorem ipsum
;; quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam
;; eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat
;; voluptatem.  Ut enim ad minima veniam, quis nostrum exercitationem
;; ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi
;; consequatur?  Quis autem vel eum @dfn{iure} reprehenderit qui in ea voluptate
;; velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum
;; fugiat quo voluptas nulla pariatur?

(provide 'section-11)
;;; section-11.el ends here
