;;; section-12.el --- Exercises for the section 12

;;; Commentary:
;; Exercises with 're-search-forward'

;;; Code:

;; 1) Write a function to search for a regular expression that matches
;; two or more blank lines in sequence.

(defun my-search-blank-lines (count)
  "Search two or more consecutive blank lines.

The argument COUNT determines the counter and direction of the
search."
  (interactive "p")
  (push-mark)
  (re-search-forward "^\n\n+" nil nil count))

;; 2) Write a function to search for duplicated words, such as “the
;; the”.  *Note Syntax of Regular Expressions: (emacs)Regexps, for
;; information on how to write a regexp (a regular expression) to
;; match a string that is composed of two identical halves.  You can
;; devise several regexps; some are better than others.  The function
;; I use is described in an appendix, along with several regexps.
;; *Note ‘the-the’ Duplicated Words Function: the-the.

(defun my-search-duplicated-words (count)
  "Search duplicated words.

Use COUNT as a counter and direction for the search."
  (interactive "p")
  (push-mark)
  (re-search-forward "\\<\\(\\w+?\\)\\>\\W+?\\<\\1\\>" nil nil count))

(provide 'section-12)
;;; section-12.el ends here
