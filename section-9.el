;;; section-9.el --- Exercises for the section 9

;;; Commentary:
;; Exercises about list implementation.

;;; Code:

;; Set ‘flowers’ to ‘violet’ and ‘buttercup’.  Cons two more flowers
;; on to this list and set this new list to ‘more-flowers’.  Set the
;; CAR of ‘flowers’ to a fish.  What does the ‘more-flowers’ list now
;; contain?

(setq flowers '(violet buttercup))
(setq more-flowers (cons 'rose (cons 'tulip flowers)))
(setcar flowers 'tuna)
;; more-flowers -> '(rose tulip tuna buttercup)

(provide 'section-9)
;;; section-9.el ends here
